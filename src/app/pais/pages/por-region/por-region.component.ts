import { Component } from '@angular/core';
import { PaisService } from '../../services/pais.service';

import { Pais } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styles: [`
    button {
      margin-right: 5px;
    }
  `]
})
export class PorRegionComponent {

  regiones: string[] = ['africa', 'americas', 'asia', 'europe', 'oceania'];
  regionActiva: string = '';
  paises: Pais[] = [];

  constructor(private paisService: PaisService) { }

  getClaseCSS( region: string ) {
    return (region === this.regionActiva) ? 'btn btn-dark' : 'btn btn-outline-dark'
  }

  activarRegion( region: string) {

    if(region === this.regionActiva) { return }

    this.regionActiva = region;
    this.paises = [];

    this.paisService.buscarRegion( region )
      .subscribe( paises => this.paises = paises);
  }

}
