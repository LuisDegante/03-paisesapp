import { Component } from '@angular/core';

import { Pais } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styles: [`
    li {
      cursor: pointer;
    }
  `]  
})
export class PorCapitalComponent {

  termino: string = '';
  hayError: boolean = false;
  paises: Pais[] = [];

  capitalesSugeridos: Pais[] = [];
  estaMostrandoSugerencias: boolean = false;

  constructor(private paisService: PaisService) { }

  buscar( termino: string ) {
    
    this.hayError = false;
    this.termino = termino;
    
    this.paisService.buscarCapital( this.termino )
      .subscribe( (paises) => {
        this.paises = paises;

      }, (err) => {
        console.info(err);
        this.hayError = true;
        this.paises = [];
      });
  }

  sugerencias( termino: string ) {
    
    this.hayError = false;
    this.termino = termino;
    this.estaMostrandoSugerencias = true;
    
    this.paisService.buscarCapital( termino )
      .subscribe( paises => {
        this.capitalesSugeridos = paises.splice(0,10);
      }, err => {
        this.capitalesSugeridos = [];
        this.estaMostrandoSugerencias = false;
      })
  }

  buscarSugerido(termino: string) {
    this.buscar( termino );    
  }
}
